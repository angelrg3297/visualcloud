<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('films', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title', 255);
            $table->string('sinopsis',255);
            $table->float('score')->nullable();
            $table->integer('chapters')->nullable();
            $table->string('duration', 50)->nullable();
            $table->string('studio', 255);
            $table->unsignedBigInteger('genre_id');
            $table->unsignedBigInteger('state_id');
            $table->date('debut');
            $table->string('image', 255);

            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();

            $table->foreign('genre_id')->references('id')->on('genres')->onUpdate('cascade');
            $table->foreign('state_id')->references('id')->on('filmstates')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('films');
    }
}
