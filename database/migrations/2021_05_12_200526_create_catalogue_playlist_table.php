<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCataloguePlaylistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalogue_playlist', function (Blueprint $table) {
            $table->unsignedBigInteger('catalogue_id');
            $table->unsignedBigInteger('playlist_id');

            $table->foreign('catalogue_id')->references('id')->on('catalogues')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('playlist_id')->references('id')->on('playlists')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catalogue_playlist');
    }
}
