<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
            /*$table->bigIncrements('id');
            $table->string('title', 255);
            $table->string('sinopsis',255);
            $table->float('score')->nullable();
            $table->integer('chapters')->nullable();
            $table->string('duration', 50)->nullable();
            $table->string('studio', 255);
            $table->unsignedBigInteger('genre_id');
            $table->unsignedBigInteger('state_id');
            $table->date('debut');*/

    public function run()
    {
        DB::table('films')->insert([
            [
                'title' => Str::random(10),
                'sinopsis' => Str::random(10),
                'score' => Str::random(10),
                'image' => Str::random(10),
                'video_path' => Str::random(10),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]
        ]);
    }
}
