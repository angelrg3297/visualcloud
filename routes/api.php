<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

//CRUD DE USERS hecho
Route::group(['middleware' => 'auth:api'],function(){
    Route::get('user/index', '\App\Http\Controllers\UserController@index');
    Route::post('user/create', '\App\Http\Controllers\UserController@create');
    Route::get('user/show/{id}', '\App\Http\Controllers\UserController@show');
    Route::delete('user/destroy/{id}', '\App\Http\Controllers\UserController@destroy');
    Route::post('user/update/{id}', '\App\Http\Controllers\UserController@update');
    Route::post('user/follow', '\App\Http\Controllers\UserController@actionFollow');
    Route::post('user/film', '\App\Http\Controllers\UserController@userFilm');
    Route::get('user/followers/{id}', '\App\Http\Controllers\UserController@getFollowers');
    Route::get('user/following/{id}', '\App\Http\Controllers\UserController@getFollowing');
});

//CRUD DE CATALOGO hecho
Route::group(['middleware' => 'auth:api'],function(){
    Route::get('catalogue/index', '\App\Http\Controllers\CatalogueController@index');
    Route::post('catalogue/create', '\App\Http\Controllers\CatalogueController@create');
    Route::get('catalogue/show/{id}', '\App\Http\Controllers\CatalogueController@show');
    Route::delete('catalogue/destroy/{id}', '\App\Http\Controllers\CatalogueController@destroy');
    Route::post('catalogue/update/{id}', '\App\Http\Controllers\CatalogueController@update');
    Route::post('catalogue/playlist', '\App\Http\Controllers\CatalogueController@actionPlaylist');
});

//CRUD DE CHAPTER hecho
Route::group(['middleware' => 'auth:api'],function(){
    Route::get('chapter/index', '\App\Http\Controllers\ChapterController@index');
    Route::post('chapter/create', '\App\Http\Controllers\ChapterController@create');
    Route::get('chapter/show/{id}', '\App\Http\Controllers\ChapterController@show');
    Route::delete('chapter/destroy/{id}', '\App\Http\Controllers\ChapterController@destroy');
    Route::post('chapter/update/{id}', '\App\Http\Controllers\ChapterController@update');
    Route::get('chapter/comments/{id}', '\App\Http\Controllers\ChapterController@getComments');
});
//CRUD DE COMMENTS hecho
Route::group(['middleware' => 'auth:api'],function(){
    Route::get('comment/index', '\App\Http\Controllers\CommentController@index');
    Route::post('comment/create', '\App\Http\Controllers\CommentController@create');
    Route::get('comment/show/{id}', '\App\Http\Controllers\CommentController@show');
    Route::delete('comment/destroy/{id}', '\App\Http\Controllers\CommentController@destroy');
    Route::post('comment/update/{id}', '\App\Http\Controllers\CommentController@update');
});

//CRUD DE FILM hecho
Route::group(['middleware' => 'auth:api'],function(){
    Route::get('film/index', '\App\Http\Controllers\FilmController@index');
    Route::post('film/create', '\App\Http\Controllers\FilmController@create');
    Route::get('film/show/{id}', '\App\Http\Controllers\FilmController@show');
    Route::delete('film/destroy/{id}', '\App\Http\Controllers\FilmController@destroy');
    Route::post('film/update/{id}', '\App\Http\Controllers\FilmController@update');
    Route::get('film/chapters/{id}', '\App\Http\Controllers\FilmController@getChapters');
    Route::get('film/reviews/{id}', '\App\Http\Controllers\FilmController@getReviews');
});
//CRUD DE PLAYLIST hecho
Route::group(['middleware' => 'auth:api'],function(){
    Route::get('playlist/index', '\App\Http\Controllers\PlayListController@index');
    Route::post('playlist/create', '\App\Http\Controllers\PlayListController@create');
    Route::get('playlist/show/{id}', '\App\Http\Controllers\PlayListController@show');
    Route::delete('playlist/destroy/{id}', '\App\Http\Controllers\PlayListController@destroy');
    Route::post('playlist/update/{id}', '\App\Http\Controllers\PlayListController@update');

});
//CRUD DE GENRE hecho
Route::group(['middleware' => 'auth:api'],function(){
    Route::get('genre/index', '\App\Http\Controllers\GenreController@index');
    Route::post('genre/create', '\App\Http\Controllers\GenreController@create');
    Route::get('genre/show/{id}', '\App\Http\Controllers\GenreController@show');
    Route::delete('genre/destroy/{id}', '\App\Http\Controllers\GenreController@destroy');
    Route::post('genre/update/{id}', '\App\Http\Controllers\GenreController@update');

});

//CRUD DE ROLE hecho
Route::group(['middleware' => 'auth:api'],function(){
    Route::get('role/index', '\App\Http\Controllers\RoleController@index');
    Route::post('role/create', '\App\Http\Controllers\RoleController@create');
    Route::get('role/show/{id}', '\App\Http\Controllers\RoleController@show');
    Route::delete('role/destroy/{id}', '\App\Http\Controllers\RoleController@destroy');
    Route::post('role/update/{id}', '\App\Http\Controllers\RoleController@update');

});
//CRUD DE STAFF hecho
Route::group(['middleware' => 'auth:api'],function(){
    Route::get('staff/index', '\App\Http\Controllers\StaffController@index');
    Route::post('staff/create', '\App\Http\Controllers\StaffController@create');
    Route::get('staff/show/{id}', '\App\Http\Controllers\StaffController@show');
    Route::delete('staff/destroy/{id}', '\App\Http\Controllers\StaffController@destroy');
    Route::post('staff/update/{id}', '\App\Http\Controllers\StaffController@update');

});
//CRUD DE REVIEW hecho
Route::group(['middleware' => 'auth:api'],function(){
    Route::get('review/index', '\App\Http\Controllers\ReviewController@index');
    Route::post('review/create', '\App\Http\Controllers\ReviewController@create');
    Route::get('review/show/{id}', '\App\Http\Controllers\ReviewController@show');
    Route::delete('review/destroy/{id}', '\App\Http\Controllers\ReviewController@destroy');
    Route::post('review/update/{id}', '\App\Http\Controllers\ReviewController@update');

});

//CRUD DE FILMSTATE hecho
Route::group(['middleware' => 'auth:api'],function(){
    Route::get('filmstate/index', '\App\Http\Controllers\FilmStateController@index');
    Route::post('filmstate/create', '\App\Http\Controllers\FilmStateController@create');
    Route::get('filmstate/show/{id}', '\App\Http\Controllers\FilmStateController@show');
    Route::delete('filmstate/destroy/{id}', '\App\Http\Controllers\FilmStateController@destroy');
    Route::post('filmstate/update/{id}', '\App\Http\Controllers\FilmStateController@update');
});

//LOGIN
Route::group(['middleware' => 'cors'], function () {
    Route::POST('register', 'App\Http\Controllers\Api\AuthController@register');
    Route::POST('registerAdmin', 'App\Http\Controllers\Api\AuthController@registerAdmin');
    Route::POST('login', 'App\Http\Controllers\Api\AuthController@login');
});

/*Route::post('new/register', 'Api\AuthController@register');

Route::post('new/login', 'Api\AuthController@login');*/
//Route::POST('index/logout', 'App\Http\Controllers\Api\AuthController@logout');

