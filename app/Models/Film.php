<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     * Los atributos que son asignables en masa.
     * El filleable te permite rellenar estos datos de forma masiva al mismo tiempo por muchos usuarios a la vez.
     * Si lo comentas solo puedes hacer una conexion al mismo tiempo.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'title',
        'sinopsis',
        'score',
        'chapters',
        'duration',
        'studio',
        'genre_id',
        'state_id',
        'debut',
        'image',
    ];

    /**
     * The attributes that should be hidden for arrays.
     * Los atributos que deben estar ocultos para las matrices.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The attributes that should be cast to native types.
     * Los atributos que se deben convertir en tipos nativos.
     *
     * @var array
     */
    protected $casts = [
        //'email_verified_at' => 'datetime',
    ];

    public function playlist(){  //N-M  1pelicula pertenece a muchas playlist.
        return $this->belongsToMany('App\Models\Playlist', 'film_playlist')
            ->withPivot('playlist_id');
    }
    public function chapter(){  //1-N  1pelicula tiene muchos capitulos.
        return $this->hasMany('App\Models\Chapter');
    }
    public function review(){  //1-N  1pelicula tiene muchos review.
        return $this->hasMany('App\Models\Review');
    }
    public function user(){  //N-M 1usuario tiene muchas peliculas.
        return $this->belongsToMany('App\Models\Film', 'user_filmscores', 'film_id', 'user_id', 'state', 'score');
    }
    public function genre(){  //1-1  1pelicula tiene un genero.
        return $this->belongsTo('App\Models\Genre');
    }
    public function filmState(){  //1-1  1pelicula tiene un estado.
        return $this->belongsTo('App\Models\FilmState');
    }

}
