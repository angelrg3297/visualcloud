<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     * Los atributos que son asignables en masa.
     * El filleable te permite rellenar estos datos de forma masiva al mismo tiempo por muchos usuarios a la vez.
     * Si lo comentas solo puedes hacer una conexion al mismo tiempo.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'chapter_id',
        'content',
        'like',
        'dislike',
    ];

    /**
     * The attributes that should be hidden for arrays.
     * Los atributos que deben estar ocultos para las matrices.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The attributes that should be cast to native types.
     * Los atributos que se deben convertir en tipos nativos.
     *
     * @var array
     */
    protected $casts = [
        //'email_verified_at' => 'datetime',
    ];

    public function chapter(){  //1-1  1Comentario pertenece a 1 capitulo.
        return $this->belongsTo('App\Models\Chapter');
    }

    public function user(){  //1-1  1Comentario pertenece a 1 usuario.
        return $this->belongsTo('App\Models\User');
    }

    public function reply(){  //1-N 1Comentario tiene muchos comentarios.
        return $this->hasMany('App\Models\Reply');
    }
    //Reply ??
}
