<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class User_filmscore extends Pivot
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'film_id',
        'state',
        'score'
    ];

    public function film(){  //1-1  1Comentario pertenece a 1 capitulo.
        return $this->belongsTo('App\Models\Film');
    }

    public function user(){  //1-1  1Comentario pertenece a 1 usuario.
        return $this->belongsTo('App\Models\User', '');
    }
}
