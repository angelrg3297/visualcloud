<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     * Los atributos que son asignables en masa.
     * El filleable te permite rellenar estos datos de forma masiva al mismo tiempo por muchos usuarios a la vez.
     * Si lo comentas solo puedes hacer una conexion al mismo tiempo.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'film_id',
        'title',
        'score',
        'season',
        'debut',
        'num_capitulo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     * Los atributos que deben estar ocultos para las matrices.
     *
     * @var array
     **/

    /**
     * The attributes that should be cast to native types.
     * Los atributos que se deben convertir en tipos nativos.
     *
     * @var array
     */
    protected $casts = [
        //'email_verified_at' => 'datetime',
    ];

    public function film(){  //1-1  1Capitulo pertenece a 1 pelicula.
        return $this->belongsTo('App\Models\Film');
    }

    public function comments(){  //1-1  1Capitulo pertenece a 1 pelicula.
        return $this->hasMany('App\Models\Comment');
    }

    public function usuario(){  //N-M  1Capitulo es visto por muchos usuarios.
        return $this->belongsToMany('App\Models\User', 'user_chapterscore')
            ->withPivot('user_id', 'score');
    }
}
