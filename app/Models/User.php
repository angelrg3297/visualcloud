<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use HasFactory, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'email',
        'password',
        'name',
        'surname',
        'borndate',
        'profilepic',
        'nickname',
        'user_id',
        'follower_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function following(){ //users that are followed by this user
        return $this->belongsToMany('App\Models\User', 'followers', 'user_id', 'follower_id');
    }
    public function followers(){  //users that follow this user
        return $this->belongsToMany('App\Models\User', 'followers', 'follower_id', 'user_id');
    }

    public function chapter(){  //N-N  1usuario tiene muchos capitulos.
        return $this->belongsToMany('App\Models\Chapter', 'user_chapterscore')
            ->withPivot('chapter_id', 'score');
    }
    public function comment(){  //1-N 1usuario tiene muchos comentarios.
        return $this->hasMany('App\Models\Comment');
    }
    public function playlist(){  //1-N 1usuario tiene muchas playlist.
        return $this->hasMany('App\Models\Playlist');
    }
    public function staff(){  //1-1 1usuario tiene un sataff.
        return $this->belongsTo('App\Models\Staff');
    }
    public function catalogue(){  //1-N 1usuario tiene muchos catalogos.
        return $this->hasMany('App\Models\Catalogue');
    }
    public function film(){  //N-M 1usuario tiene muchas peliculas.
        return $this->belongsToMany('App\Models\Film', 'user_filmscores', 'user_id', 'film_id', 'state', 'score');
    }
    public function review(){  //1-N 1usuario tiene muchas review.
        return $this->hasMany('App\Models\Review');
    }
    public function roles(){  //N-M  1usuario tiene muchos roles.
        return $this->belongsToMany('App\Models\Roles', 'roles_user')
            ->withPivot('roles_id');
    }
}
