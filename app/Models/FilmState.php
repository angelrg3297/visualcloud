<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Filmstate extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     * Los atributos que son asignables en masa.
     * El filleable te permite rellenar estos datos de forma masiva al mismo tiempo por muchos usuarios a la vez.
     * Si lo comentas solo puedes hacer una conexion al mismo tiempo.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
    ];

    public $timestamps = false;


    /**
     * The attributes that should be cast to native types.
     * Los atributos que se deben convertir en tipos nativos.
     *
     * @var array
     */
    protected $casts = [
        //'email_verified_at' => 'datetime',
    ];

    public function film(){  //1-N  1state tiene muchas peliculas.
        return $this->hasMany('App\Models\Film');
    }
}
