<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Staff;
class StaffController extends Controller
{
    public function index()
    {
      
        $allStaffs=Staff::select("staff.*")->get()->toArray();
        return response()->json($allStaffs);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $newStaff =$request->all();
        $validator =Validator::make($newStaff,[
         'user_id'=>'required',
        'deathdate'=>'required',
         'birthplace'=>'required'

        ]); 

        if($validator->fails()){
            return response()->json([
                'ok' => false,
                'error' => $validator->messages(),
            ]);
        }

            try{
                Staff::create($newStaff);
                return response()->json([
                 'ok' => true,
                 'mensaje' => "Se creó con exito",
             ]);
            } catch(\Exception $e){
             return response()->json([
                 'ok' => false,
                 'error' => $e->getMessage(),
             ]);
            }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Staff  $Staff
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    
        $Staffs=Staff::select("staff.*")
            ->where("staff.id",$id)
            ->first();
            return response()->json([
                'ok' => true,
                'data' => $Staffs,
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Staff  $Staff
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
 
        try{
            $oneStaff=Staff::find($id);
            if ($oneStaff==false){
                return response()->json([
                    'ok' => false,
                    'data' => "No se encontro el staff",
                ]);
            }
            $oneStaff->update();
            return response()->json([
                'staff' => $oneStaff,
            ]);
        }catch(\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
       } 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Staff  $Staff
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[ 
            'user_id'=>'required',
            'deathdate'=>'required',
            'birthplace'=>'required'
        ]);
        try{
            Staff::find($id)->update($request->all());
            return response()->json([
                'ok' => true,
                'data' => "Se actualizo con exito",
            ]);
        }catch (\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Staff  $Staff
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try{
            $Staffs=Staff::find($id);
            if ($Staffs==false){
                return response()->json([
                    'ok' => false,
                    'data' => "No se encontro el usuario",
                ]);
            }
                $Staffs->delete([
                ]);
                return response()->json([
                    'ok' => true,
                    'data' => "Se elimino con exito",
                ]);
        }catch(\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
           } 
    }
}