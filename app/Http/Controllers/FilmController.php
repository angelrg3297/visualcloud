<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Film;
class FilmController extends Controller
{
    public function index()
    {
      
        $allfilms=Film::select("films.*")->get()->toArray();
        return response()->json($allfilms);

    }

    public function getChapters($id){
        $film=Film::find($id);
        $chapters=$film->chapter()->get();
        return response()->json([
            'data' => $chapters,
            'ok' => true
        ]);
    }

    public function getReviews($id){
        $film=Film::find($id);
        $reviews=$film->review()->get();
        return response()->json([
            'data' => $reviews,
            'ok' => true
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create(Request $request)
    {

        $newFilm =$request->all();
       // $newFilm->title->$request->input('title');
        //$newFilm->score->$request->input('score');
        //$newFilm->chapters->$request->input('chapters');

        $validator = Validator::make($newFilm,[
         'title'=>'required|max:70',
        'sinopsis'=>'required|max:70',
         'score'=>'max:255|required',
         'chapters'=>'required',
         'duration' => 'max:255',
        'studio' => 'max:255',
        'genre_id' => 'max:255',
        'state_id' => 'max:255',
        'debut' => 'max:255',
        'image' => 'max:255'
        ]); 

        if($validator->fails()){
            return response()->json([
                'ok' => false,
                'error' => $validator->messages(),
            ]);
        }

            try{
                Film::create($newFilm);
                return response()->json([
                 'ok' => true,
                 'mensaje' => "Se creó con exito",
             ]);
            } catch(\Exception $e){
             return response()->json([
                 'ok' => false,
                 'error' => $e->getMessage(),
             ]);
            }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Film  $Film
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    
        $films=Film::select("films.*")
            ->where("films.id",$id)
            ->first();
            return response()->json([
                'ok' => true,
                'data' => $films,
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Film  $Film
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
 
        try{
            $oneFilm=Film::find($id);
            if ($oneFilm==false){
                return response()->json([
                    'ok' => false,
                    'data' => "No se encontro el usuario",
                ]);
            }
            $oneFilm->update();
            return response()->json([
                'Film' => $oneFilm,
            ]);
        }catch(\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
       } 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Film  $Film
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title'=>'required|max:70',
            'sinopsis'=>'required|max:70',
            'score'=>'max:255|required',
            'chapters'=>'required',
            'duration' => 'max:255',
            'studio' => 'max:255',
            'genre_id' => 'max:255',
            'state_id' => 'max:255',
            'debut' => 'max:255',
            'image' => 'max:255'
        ]);
        try{
            Film::find($id)->update($request->all());
            return response()->json([
                'ok' => true,
                'data' => "Se actualizo con exito",
            ]);
        }catch (\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Film  $Film
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try{
            $films=Film::find($id);
            if ($films==false){
                return response()->json([
                    'ok' => false,
                    'data' => "No se encontro el usuario",
                ]);
            }
                $films->delete([
                ]);
                return response()->json([
                    'ok' => true,
                    'data' => "Se elimino con exito",
                ]);
        }catch(\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
           } 
    }
}
