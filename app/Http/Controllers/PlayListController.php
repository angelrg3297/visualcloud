<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Playlist;
class PlaylistController extends Controller
{
    public function index()
    {
      
        $allplaylist=Playlist::select("playlists.*")->get()->toArray();
        return response()->json($allplaylist);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $newPlaylist =$request->all();
        $validator =Validator::make($newPlaylist,[
         'user_id'=>'required',
        'title'=>'required',
         'description'=>'max:255|required',
         'image'=>'required'
        ]); 

        if($validator->fails()){
            return response()->json([
                'ok' => false,
                'error' => $validator->messages(),
            ]);
        }

            try{
                Playlist::create($newPlaylist);
                return response()->json([
                 'ok' => true,
                 'mensaje' => "Se creó con exito",
             ]);
            } catch(\Exception $e){
             return response()->json([
                 'ok' => false,
                 'error' => $e->getMessage(),
             ]);
            }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Playlist  $Playlist
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    
        $playlist=Playlist::select("playlists.*")
            ->where("playlists.id",$id)
            ->first();
            return response()->json([
                'ok' => true,
                'data' => $playlist,
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Playlist  $Playlist
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
 
        try{
            $onePlaylist=Playlist::find($id);
            if ($onePlaylist==false){
                return response()->json([
                    'ok' => false,
                    'data' => "No se encontro el usuario",
                ]);
            }
            $onePlaylist->update();
            return response()->json([
                'Playlist' => $onePlaylist,
            ]);
        }catch(\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
       } 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Playlist  $Playlist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'user_id'=>'required',
            'title'=>'required',
            'description'=>'max:255|required',
            'image'=>'required']
        );
        try{
            Playlist::find($id)->update($request->all());
            return response()->json([
                'ok' => true,
                'data' => "Se actualizo con exito",
            ]);
        }catch (\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Playlist  $Playlist
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try{
            $playlist=Playlist::find($id);
            if ($playlist==false){
                return response()->json([
                    'ok' => false,
                    'data' => "No se encontro el usuario",
                ]);
            }
                $playlist->delete([
                ]);
                return response()->json([
                    'ok' => true,
                    'data' => "Se elimino con exito",
                ]);
        }catch(\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
           } 
    }
}