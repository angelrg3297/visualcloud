<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use App\Models\User;
use App\Models\Catalogue;
use App\Models\Roles;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    /**
     * 
     * @param Request Request http request object
     * 
     * @return json
     */
    public function login(Request $request)
    {
        $loginData = $request->validate([
            'email'=>'email|required',
            'password'=>'required'
        ]);

        if (!auth()->attempt($loginData)) {
            return response()->json('Usuario o contraseña invalidos', 400);// Bad request
        }
        $data = $request->all();
        $user=User::where('email', $data['email'])->first();
        $roles=$user->roles()->get();
        //oK encontre el usuario TODO retornan el token
        $accessToken = auth()->user()->createToken('authToken')->accessToken;
        return response()->json(['user'=>auth()->user(), 'roles' => $roles, 'access_token'=>$accessToken]);
    }

    /**
     * Registro
     * 
     * @param Request $request
     * @return json
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(),
        [
            'role' => 'required',
            'nickname' => 'required|max:70',
            'name' => 'required|max:70',
            'surname' => 'required|max:70',
            'email' => 'email|required',
            'password' => 'required',
            'borndate' => 'date',
            'profilepic' => 'max:255'
        ]);

        $loginData = $request->validate([
            'email'=>'email|required',
            'password'=>'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['msg' => 'No es posible realizar el registro', 'status' => 400, 'errors' => $validator->errors()]);
        }
        $data = $request->all();
//Checkear si el email del usuario ya existe
        $userExists = User::where('email', '=', $data['email'])->first();
        if ($userExists === null){
//Entonces lo crea
            try {
                $user = new User();
                $data['password'] = Hash::make($data['password']);
                $user->create($data);
                $accessToken = $user->createToken('authToken')->accessToken;
//Asignar role de user
                //Comprobamos si el role es un dato numerico o string para buscarlo en la tabla
                if (is_numeric($data['role'])){
                    $role = Roles::where('id', '=', $data['role'])->first();//Busca el id
                }else{
                    $role = Roles::where('name', '=', $data['role'])->first();//Busca el name
                }
                $userWithRole = User::where('email', '=', $data['email'])->first(); //Buscamos el user de la tabla
                $userWithRole -> roles() -> attach($role->getKey()); //Al user le hacemos la relacion con su rol
//Crear Catalogo de user
                $dataCatalogue = [
                    "user_id" => $userWithRole->getKey(),
                    "name" => "Main Catalogue"
                ];
                $catalogue = new Catalogue();
                $catalogue::create($dataCatalogue);
            } catch (\Throwable $th) {
                dd($th->getMessage());
                return response()->json(['user' => null, 'access_token' => null, 'status' => 500, 'msg' => 'No se pudo crear el usuario']);
            }
            if (!auth()->attempt($loginData)) {
                return response()->json('Fatal Error', 400);
            }
            return response()->json(['user' => $userWithRole, 'role' => $role->name, 'access_token' => $accessToken, 'status' => 201]);
        }else{
            return response()->json(['user' => null, 'access_token' => null, 'status' => 409, 'msg' => 'El usuario ya existe']);
        }
    }

    public function logout(Request $request){
        $request->user()->token()->revoke();
        return response()->json(['message'=> 'Deslogueado correctamente']);
     }

     public function user(Request $request){
        return response()->json($request->user());
     }
}