<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Genre;
class GenreController extends Controller
{
    public function index()
    {
      
        $allGenre=Genre::select("Genres.*")->get()->toArray();
        return response()->json($allGenre);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $newGenre =$request->all();
       // $newGenre->title->$request->input('title');
        //$newGenre->score->$request->input('score');
        //$newGenre->chapters->$request->input('chapters');

        $validator =Validator::make($newGenre,[
         'name'=>'required|max:70',
         
        
        ]); 

        if($validator->fails()){
            return response()->json([
                'ok' => false,
                'error' => $validator->messages(),
            ]);
        }

            try{
                Genre::create($newGenre);
                return response()->json([
                 'ok' => true,
                 'mensaje' => "Se creó con exito",
             ]);
            } catch(\Exception $e){
             return response()->json([
                 'ok' => false,
                 'error' => $e->getMessage(),
             ]);
            }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Genre  $Genre
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    
        $Genre=Genre::select("Genres.*")
            ->where("Genres.id",$id)
            ->first();
            return response()->json([
                'ok' => true,
                'data' => $Genre,
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Genre  $Genre
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
 
        try{
            $oneGenre=Genre::find($id);
            if ($oneGenre==false){
                return response()->json([
                    'ok' => false,
                    'data' => "No se encontro el genre",
                ]);
            }
            $oneGenre->update();
            return response()->json([
                'Genre' => $oneGenre,
            ]);
        }catch(\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
       } 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Genre  $Genre
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[ 'name'=>'required']);
        try{
            Genre::find($id)->update($request->all());
            return response()->json([
                'ok' => true,
                'data' => "Se actualizo con exito",
            ]);
        }catch (\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Genre  $Genre
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try{
            $Genre=Genre::find($id);
            if ($Genre==false){
                return response()->json([
                    'ok' => false,
                    'data' => "No se encontro el genre",
                ]);
            }
                $Genre->delete([
                ]);
                return response()->json([
                    'ok' => true,
                    'data' => "Se elimino con exito",
                ]);
        }catch(\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
           } 
    }
}
