<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Chapter;
class ChapterController extends Controller
{
    public function index()
    {
      
        $allchapters=Chapter::select("chapters.*")->get()->toArray();
        return response()->json($allchapters);

    }

    public function getComments($id){
        $chapter=Chapter::find($id);
        $comments=$chapter->comments()->get();
        return response()->json([
            'data' => $comments,
            'ok' => true
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $newChapter =$request->all();
       // $newChapter->name->$request->input('name');
        //$newChapter->email->$request->input('email');
        //$newChapter->password->$request->input('password');

        $validator =Validator::make($newChapter,[
            'film_id'=>'required',
            'title'=>'required',
            'score'=>'max:255',
            'season'=>'required',
            'debut' => 'max:20',
            'num_capitulo'=>'max:11'
        ]); 

        if($validator->fails()){
            return response()->json([
                'ok' => false,
                'error' => $validator->messages(),
            ]);
        }

            try{
                Chapter::create($newChapter);
                return response()->json([
                 'ok' => true,
                 'mensaje' => "Se creó con exito",
             ]);
            } catch(\Exception $e){
             return response()->json([
                 'ok' => false,
                 'error' => $e->getMessage(),
             ]);
            }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Chapter  $Chapter
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    
        $chapters=Chapter::select("chapters.*")
            ->where("chapters.id",$id)
            ->first();
            return response()->json([
                'ok' => true,
                'data' => $chapters,
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Chapter  $Chapter
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
 
        try{
            $oneChapter=Chapter::find($id);
            if ($oneChapter==false){
                return response()->json([
                    'ok' => false,
                    'data' => "No se encontro el usuario",
                ]);
            }
            $oneChapter->update();
            return response()->json([
                'Chapter' => $oneChapter,
            ]);
        }catch(\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
       } 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Chapter  $Chapter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'film_id'=>'required',
            'title'=>'required',
            'score'=>'max:255',
            'season'=>'required',
            'debut' => 'max:20',
            'num_capitulo'=>'max:11'
        ]);
        try{
            Chapter::find($id)->update($request->all());
            return response()->json([
                'ok' => true,
                'data' => "Se actualizo con exito",
            ]);
        }catch (\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Chapter  $Chapter
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try{
            $chapters=Chapter::find($id);
            if ($chapters==false){
                return response()->json([
                    'ok' => false,
                    'data' => "No se encontro el usuario",
                ]);
            }
                $chapters->delete([
                ]);
                return response()->json([
                    'ok' => true,
                    'data' => "Se elimino con exito",
                ]);
        }catch(\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
           } 
    }
}
