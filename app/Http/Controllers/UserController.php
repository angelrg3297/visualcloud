<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Comment;
use App\Models\Catalogue;
use App\Models\Roles;
use App\Models\Film;
use App\Models\User_filmscore;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
    
        $allUsers=User::select("users.*")->get()->toArray();
        return response()->json($allUsers);

    }
//Puntuar pelicula
    public function userFilm(Request $request){
        $data = $request->all();
        $validator=Validator::make($data,[
            'user_id' => 'required',
            'film_id' => 'required',
            'state' => 'required',
            'score' => 'nullable'
        ]); 
        if($validator->fails()){
            return response()->json([
                'ok' => false,
                'error' => $validator->messages(),
            ]);
        }
        $film = Film::find($data['film_id']);
        $user = User::find($data['user_id']);
        $filmId=$data['film_id'];
        $userId=$data['user_id'];
//Checkear si el usuario ya está relacionado con la pelicula
        /*$hasFilm = User::where('id', $data['user_id'])->whereHas('film', function ($q) use ($filmId) {
            $q->where('id', $filmId);
        })->exists();*/
        //$hasFilm = $user->film()->where('film_id', '=', $filmId)->exists();
        var_dump($user->film()->sync($filmId));
        /*if(User::where('id', $userId)->where('id')){
            $user->film()->attach($film->getKey(), [
                'film_id' => $data['film_id'],
                'user_id' => $data['user_id'],
                'state' => $data['state'],
                'score'=> $data['score']
            ]);
        }else{
            $user->film()->detach($film->getKey());
        }*/
        die();
        //var_dump($database);
        
    }
//Ver los seguidores
    public function getFollowers($id){
        $user=User::find($id);
        $followers=$user->followers()->get();
        return response()->json([
            'data' => $followers,
            'ok' => true
        ]);
    }
//Ver los seguidos
    public function getFollowing($id){
        $user=User::find($id);
        $following=$user->following()->get();
        return response()->json([
            'data' => $following,
        ]);
    }
//Seguir / dejar de seguir usuario
    public function actionFollow(Request $request){
        $data = $request->all();
        $validator=Validator::make($data,[
            'user_id' => 'required',
            'follow_id' => 'required',
            'action' => 'required'
        ]); 

        if($validator->fails()){
            return response()->json([
                'ok' => false,
                'error' => $validator->messages(),
            ]);
        }

        $loggedUser = User::find($data['user_id']);
        $followedUser = User::find($data['follow_id']);
        //Saber si quiere seguir, o dejar de seguir
        if($data['action']=='follow'){
            $loggedUser->following()->attach($followedUser);
        }else{
            $loggedUser->following()->detach($followedUser);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data = $request->all();
        $validator=Validator::make($data,[
            'role' => 'required',
            'nickname'=>'required',
            'name'=>'required',
            'surname'=>'required',
            'email'=>'email|required',
            'password'=>'required',
            'borndate' => 'max:20',
            'profilepic' => 'max:255'
        ]); 

        if($validator->fails()){
            return response()->json([
                'ok' => false,
                'error' => $validator->messages(),
            ]);
        }
//Checkear si el email del usuario ya existe
        $userExists = User::where('email', '=', $data['email'])->first();
        if ($userExists === null){
    //Entonces lo crea
            try{
                $user = new User();
                $data['password'] = Hash::make($data['password']);
                $user->create($data);
    //Asignar role de user
                //Comprobamos si el role es un dato numerico o string para buscarlo en la tabla
                if (is_numeric($data['role'])){
                    $role = Roles::where('id', '=', $data['role'])->first();//Busca el id
                }else{
                    $role = Roles::where('name', '=', $data['role'])->first();//Busca el name
                }
                $RolesUser = User::where('email', '=', $data['email'])->first(); //Buscamos el user de la tabla
                $RolesUser -> roles() -> attach($role->getKey()); //Al user le hacemos la relacion con su rol
    //Crear Catalogo de user
                /*$newCatalogue = [
                    "user_id" => $RolesUser->getKey(),
                    "name" => "Main Catalogue"
                ];
                Catalogue::create($newCatalogue);*/
            } catch(\Exception $e){
                return response()->json([
                    'ok' => false,
                    'error' => $e->getMessage(),
                ]);
            }
        }else{
            return response()->json([
                'ok' => true,
                'mensaje' => "El usuario ya existe",
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return $users = User::findOrFail($id);
        }catch (\Exception $e) {
            return response()->json(array('msg' => 'No se ha podido mostrar el usuario', 'status' => 'false'), 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
 
        try{
            $oneUser=User::find($id);
            if ($oneUser==false){
                return response()->json([
                    'ok' => false,
                    'data' => "No se encontro el usuario",
                ]);
            }
            $oneUser->update();
            return response()->json([
                'user' => $oneUser,
            ]);
        }catch(\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
       } 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'role' => 'nullable',
            'nickname'=>'required',
            'name'=>'required',
            'surname'=>'required',
            'email'=>'email|required',
            'password'=>'required',
            'borndate' => 'max:20',
            'profilepic' => 'max:255'
        ]);
        try{
            $user = new User();
            $data = $request->all();
            $data['password'] = Hash::make($data['password']);
            $user::find($id)->update($data);
            
            return response()->json([
                'ok' => true,
                'data' => "Se actualizo con exito",
            ]);
        }catch (\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
        }
//Asignar role de user
        //Comprobamos si el role es un dato numerico o string para buscarlo en la tabla
        if (is_numeric($data['role'])){
            $role = Roles::where('id', '=', $data['role'])->first();//Busca el id
        }else{
            $role = Roles::where('name', '=', $data['role'])->first();//Busca el name
        }
            $RolesUser = User::where('email', '=', $data['email'])->first(); //Buscamos el user de la tabla
            $RolesUser -> roles() -> attach($role->getKey()); //Al user le hacemos la relacion con su rol
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try{
            $users=User::find($id);
            if ($users==false){
                return response()->json([
                    'ok' => false,
                    'data' => "No se encontro el usuario",
                ]);
            }
                $users->delete([
                ]);
                return response()->json([
                    'ok' => true,
                    'data' => "Se elimino con exito",
                ]);
        }catch(\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
           } 
    }
}
