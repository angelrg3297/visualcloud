<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Review;
class ReviewController extends Controller
{
    public function index()
    {
      
        $allReviews=Review::select("reviews.*")->get()->toArray();
        return response()->json($allReviews);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $newReview =$request->all();

        $validator =Validator::make($newReview,[
         'user_id'=>'required',
         'film_id'=>'required',
         'content'=>'required',
         'useful_for'=>'max:11'
        ]); 

        if($validator->fails()){
            return response()->json([
                'ok' => false,
                'error' => $validator->messages(),
            ]);
        }

            try{
                Review::create($newReview);
                return response()->json([
                 'ok' => true,
                 'mensaje' => "Se creó con exito",
             ]);
            } catch(\Exception $e){
             return response()->json([
                 'ok' => false,
                 'error' => $e->getMessage(),
             ]);
            }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Review  $Review
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    
        $Reviews=Review::select("reviews.*")
            ->where("reviews.id",$id)
            ->first();
            return response()->json([
                'ok' => true,
                'data' => $Reviews,
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Review  $Review
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
 
        try{
            $oneReview=Review::find($id);
            if ($oneReview==false){
                return response()->json([
                    'ok' => false,
                    'data' => "No se encontro el review",
                ]);
            }
            $oneReview->update();
            return response()->json([
                'Review' => $oneReview,
            ]);
        }catch(\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
       } 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Review  $Review
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'user_id'=>'required',
            'film_id'=>'required',
            'content'=>'required',
            'useful_for'=>'max:11'
        ]);
        try{
            Review::find($id)->update($request->all());
            return response()->json([
                'ok' => true,
                'data' => "Se actualizo con exito",
            ]);
        }catch (\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Review  $Review
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try{
            $Reviews=Review::find($id);
            if ($Reviews==false){
                return response()->json([
                    'ok' => false,
                    'data' => "No se encontro el review",
                ]);
            }
                $Reviews->delete([
                ]);
                return response()->json([
                    'ok' => true,
                    'data' => "Se elimino con exito",
                ]);
        }catch(\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
           } 
    }
}
