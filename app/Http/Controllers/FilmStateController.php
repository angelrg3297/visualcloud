<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Filmstate;
class FilmstateController extends Controller
{
    public function index()
    {
      
        $allFilmStates=FilmState::select("filmstates.*")->get()->toArray();
        return response()->json($allFilmStates);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $newFilmState =$request->all();


        $validator =Validator::make($newFilmState,[
         'name'=>'required|max:70'
        
        ]); 

        if($validator->fails()){
            return response()->json([
                'ok' => false,
                'error' => $validator->messages(),
            ]);
        }

            try{
                FilmState::create($newFilmState);
                return response()->json([
                 'ok' => true,
                 'mensaje' => "Se creó con exito",
             ]);
            } catch(\Exception $e){
             return response()->json([
                 'ok' => false,
                 'error' => $e->getMessage(),
             ]);
            }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FilmState  $FilmState
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    
        $FilmState=FilmState::select("filmstates.*")
            ->where("filmstates.id",$id)
            ->first();
            return response()->json([
                'ok' => true,
                'data' => $FilmState,
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FilmState  $FilmState
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
 
        try{
            $oneFilmState=FilmState::find($id);
            if ($oneFilmState==false){
                return response()->json([
                    'ok' => false,
                    'data' => "No se encontro el usuario",
                ]);
            }
            $oneFilmState->update();
            return response()->json([
                'filmstate' => $oneFilmState,
            ]);
        }catch(\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
       } 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FilmState  $FilmState
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[ 'name'=>'required']);
        try{
            FilmState::find($id)->update($request->all());
            return response()->json([
                'ok' => true,
                'data' => "Se actualizo con exito",
            ]);
        }catch (\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FilmState  $FilmState
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try{
            $FilmState=FilmState::find($id);
            if ($FilmState==false){
                return response()->json([
                    'ok' => false,
                    'data' => "No se encontro el usuario",
                ]);
            }
                $FilmState->delete([
                ]);
                return response()->json([
                    'ok' => true,
                    'data' => "Se elimino con exito",
                ]);
        }catch(\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
           } 
    }
}
