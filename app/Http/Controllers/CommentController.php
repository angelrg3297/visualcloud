<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Comment;
class CommentController extends Controller
{
    //

    public function index()
    {
        $allComments=Comment::select("comments.*")->get()->toArray();
        return response()->json($allComments);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $input =$request->all();
        $validator =Validator::make($input,[
        'user_id'=>'required',
        'chapter_id'=>'required',
        'content'=>'required|max:255',
        'like' => 'max:6',
        'dislike' => 'max:6',

        ]);
        if($validator->fails()){
            return response()->json([
                'ok' => false,
                'error' => $validator->messages(),
            ]);
        }           
            try{
                Comment::create($input);
                return response()->json([
                'ok' => true,
                'mensaje' => "Se creó con exito",
            ]);
            } catch(\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
            }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comments=Comment::select("comments.*")
        ->where("comments.id",$id)
        ->first();
        return response()->json([
            'ok' => true,
            'data' => $comments,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        try{
            $oneComment=Comment::find($id);
            if ($oneComment==false){
                return response()->json([
                    'ok' => false,
                    'data' => "No se encontro el Commentario",
                ]);
            }
            $oneComment->update();
            return response()->json([
                'Comment' => $oneComment,
            ]);
        }catch(\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
    } 

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'content'=>'required|max:255',
            'like' => 'max:6',
            'dislike' => 'max:6',
        ]);
        try{
            Comment::find($id)->update($request->all());
            return response()->json([
                'ok' => true,
                'data' => "Se actualizo con exito",
            ]);
        }catch (\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $comments=Comment::find($id);
            if ($comments==false){
                return response()->json([
                    'ok' => false,
                    'data' => "No se encontro el usuario",
                ]);
            }
                $comments->delete([
                ]);
                return response()->json([
                    'ok' => true,
                    'data' => "Se elimino con exito",
                ]);
        }catch(\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
        } 
    }
}
