<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Roles;
class RoleController extends Controller
{
    public function index()
    {
      
        $allRoles=Roles::select("roles.*")->get()->toArray();
        return response()->json($allRoles);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $newRole =$request->all();
        $validator =Validator::make($newRole,[
         'name'=>'required|max:70',
         
        
        ]); 

        if($validator->fails()){
            return response()->json([
                'ok' => false,
                'error' => $validator->messages(),
            ]);
        }

            try{
                Roles::create($newRole);
                return response()->json([
                 'ok' => true,
                 'mensaje' => "Se creó con exito",
             ]);
            } catch(\Exception $e){
             return response()->json([
                 'ok' => false,
                 'error' => $e->getMessage(),
             ]);
            }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Roles  $Roles
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    
        $Role=Roles::select("roles.*")
            ->where("roles.id",$id)
            ->first();
            return response()->json([
                'ok' => true,
                'data' => $Role,
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Roles  $Roles
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
 
        try{
            $oneRole=Roles::find($id);
            if ($oneRole==false){
                return response()->json([
                    'ok' => false,
                    'data' => "No se encontro el usuario",
                ]);
            }
            $oneRole->update();
            return response()->json([
                'Role' => $oneRole,
            ]);
        }catch(\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
       } 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Role  $Role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[ 'name'=>'required']);
        try{
            Roles::find($id)->update($request->all());
            return response()->json([
                'ok' => true,
                'data' => "Se actualizo con exito",
            ]);
        }catch (\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Roles  $Roles
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try{
            $Role=Roles::find($id);
            if ($Role==false){
                return response()->json([
                    'ok' => false,
                    'data' => "No se encontro el usuario",
                ]);
            }
                $Role->delete([
                ]);
                return response()->json([
                    'ok' => true,
                    'data' => "Se elimino con exito",
                ]);
        }catch(\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
           } 
    }
}
