<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Catalogue;
use App\Models\Playlist;
class CatalogueController extends Controller
{
    public function index()
    {
      
        $allcatalogues=Catalogue::select("catalogues.*")->get()->toArray();
        return response()->json($allcatalogues);

    }

    public function actionPlaylist(Request $request){
        $validator = Validator::make($request->all(),
        [
            'playlist_id' => 'required',
            'user_id' => 'required',
            'action' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['msg' => 'No es posible seguir la playlist', 'status' => 400, 'errors' => $validator->errors()]);
        }
        $data = $request->all();
        $user_id=$data['user_id'];
        $playlist_id=$data['playlist_id'];

        $catalogueUser = Catalogue::where('user_id', '=', $user_id)->first(); //Buscamos el catalogue de la tabla
//Saber si quiere seguir, o dejar de seguir
        if($data['action']=='follow'){
            $catalogueUser -> playlist() -> attach($playlist_id);
        }else{
            $catalogueUser -> playlist() -> detach($playlist_id);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $newCatalogue =$request->all();

        $validator = Validator::make($newCatalogue,[
        'user_id'=>'required',
        'name'=>'required'
        ]); 

        if($validator->fails()){
            return response()->json([
                'ok' => false,
                'error' => $validator->messages(),
            ]);
        }

            try{
                Catalogue::create($newCatalogue);
                return response()->json([
                 'ok' => true,
                 'mensaje' => "Se creó con exito",
             ]);
            } catch(\Exception $e){
             return response()->json([
                 'ok' => false,
                 'error' => $e->getMessage(),
             ]);
            }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Catalogue  $Catalogue
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    
        $catalogues=Catalogue::select("catalogues.*")
            ->where("catalogues.id",$id)
            ->first();
            return response()->json([
                'ok' => true,
                'data' => $catalogues,
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Chapter  $Chapter
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
 
        try{
            $oneCatalogue=Catalogue::find($id);
            if ($oneCatalogue==false){
                return response()->json([
                    'ok' => false,
                    'data' => "No se encontro el catalogo",
                ]);
            }
            $oneCatalogue->update();
            return response()->json([
                'Chapter' => $oneCatalogue,
            ]);
        }catch(\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
       } 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Chapter  $Chapter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[ 'user_id'=>'required',
        'name'=>'required']);
        try{
            Catalogue::find($id)->update($request->all());
            return response()->json([
                'ok' => true,
                'data' => "Se actualizo con exito",
            ]);
        }catch (\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Chapter  $Chapter
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try{
            $catalogues=Catalogue::find($id);
            if ($catalogues==false){
                return response()->json([
                    'ok' => false,
                    'data' => "No se encontro el catalogo",
                ]);
            }
                $catalogues->delete([
                ]);
                return response()->json([
                    'ok' => true,
                    'data' => "Se elimino con exito",
                ]);
        }catch(\Exception $e){
            return response()->json([
                'ok' => false,
                'error' => $e->getMessage(),
            ]);
           } 
    }
}
