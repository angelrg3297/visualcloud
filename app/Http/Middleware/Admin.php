<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        
        if (Auth::check() && Auth::roles()->name!='2') {
            
            return response()->json(['error', 'El usuario no tiene permisos'], 403);
    }
    return $next($request);
    }
}
